#include <SoftwareSerial.h>

const long baud = 38400L;

const int rx = 5;
const int tx = 6;

SoftwareSerial bluetooth(rx,tx);

void setup()
{
    Serial.begin(baud);
    bluetooth.begin(baud);
}

void loop()
{
    String data = "";
    for (int i = 0; i < 10; i++)
        data += "v" + String(i) + ":" + String(analogRead(A0)) + ",";
    data.setCharAt(data.length() - 1, ';');

    bluetooth.write(data.c_str());
    Serial.println(data);
    delay(1000);
}